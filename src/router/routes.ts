import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MenuLayout.vue'),
    children: [
      { path: '', component: () => import('src/pages/IndexPage.vue') },
      { path: '/boards', component: () => import('pages/TheBoards.vue') },
      { path: '/boards1', component: () => import('pages/TheBoards1.vue') },
      { path: '/boards2', component: () => import('pages/TheBoards2.vue') },
      { path: '/boards3', component: () => import('pages/TheBoards3.vue') },
      { path: '/new-board', component: () => import('pages/TheNewBoard.vue') },
      { path: '/help', component: () => import('pages/TheHelp.vue') },
      {
        path: '/acc-settings',
        component: () => import('pages/TheAccountSettings.vue'),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
