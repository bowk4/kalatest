export interface StMenuItam {
  displayName: string;
  icon: string;
  link: { path: string };
}

export const menuItems: StMenuItam[] = [
  {
    displayName: 'New board',
    icon: 'control_point',
    link: { path: '/new-board' },
  },
];

export const menuItems2: StMenuItam[] = [
  {
    displayName: 'Help',
    icon: 'help_outline',
    link: { path: '/help' },
  },
  {
    displayName: 'Account Settings',
    icon: 'settings',
    link: { path: '/acc-settings' },
  },
];

export const subMenuItems: StMenuItam[] = [
  {
    displayName: 'Board1',
    icon: 'dashboard',
    link: { path: '/boards1' },
  },
  {
    displayName: 'Board2',
    icon: 'dashboard',
    link: { path: '/boards2' },
  },
  {
    displayName: 'Board3',
    icon: 'dashboard',
    link: { path: '/boards3' },
  },

];
